//
//  QuestionData.swift
//  Quizzler-iOS13
//
//  Created by Akhmad Harry Susanto on 21/04/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct questionData {
    let quiz = [
    Question(q: "Which is the largest organ in the human body?", a: ["Heart", "Skin", "Large Intestine"], correctAnswer: "Skin"),
    Question(q: "Five dollars is worth how many nickels?", a: ["25", "50", "100"], correctAnswer: "100"),
    Question(q: "What do the letters in the GMT time zone stand for?", a: ["Global Meridian Time", "Greenwich Mean Time", "General Median Time"], correctAnswer: "Greenwich Mean Time"),
    Question(q: "What is the French word for 'hat'?", a: ["Chapeau", "Écharpe", "Bonnet"], correctAnswer: "Chapeau"),
    Question(q: "In past times, what would a gentleman keep in his fob pocket?", a: ["Notebook", "Handkerchief", "Watch"], correctAnswer: "Watch"),
    Question(q: "How would one say goodbye in Spanish?", a: ["Au Revoir", "Adiós", "Salir"], correctAnswer: "Adiós"),
    Question(q: "Which of these colours is NOT featured in the logo for Google?", a: ["Green", "Orange", "Blue"], correctAnswer: "Orange"),
    Question(q: "What alcoholic drink is made from molasses?", a: ["Rum", "Whisky", "Gin"], correctAnswer: "Rum"),
    Question(q: "What type of animal was Harambe?", a: ["Panda", "Gorilla", "Crocodile"], correctAnswer: "Gorilla"),
    Question(q: "Where is Tasmania located?", a: ["Indonesia", "Australia", "Scotland"], correctAnswer: "Australia")
    ]
    
    var questionNumberData = 0
    var score = 0
    
    
    func getQuestionText() -> String {
        return quiz[questionNumberData].questionText
    }
    
    mutating func nextQuestion() {
        if questionNumberData + 1 < quiz.count {
            questionNumberData += 1
        } else {
            // Will Reset
            questionNumberData = 0
            score = 0
        }
    }
    
    mutating func checkAnswer(answer checkAnswer : String) -> Bool {
        if checkAnswer == quiz[questionNumberData].corAnswer {
            score += 1
            return true
        } else {
            return false
        }
    }
    
    func getProgressBar() -> Float {
        return Float (questionNumberData + 1) / Float (quiz.count)
    }
    
    func getAnswerText() -> [String] {
        return quiz[questionNumberData].answerText
    }
    
    func getScore() -> Int {
        return score
    }
    
    
}
