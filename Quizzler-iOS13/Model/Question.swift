//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by Akhmad Harry Susanto on 21/04/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Question {
    var questionText : String
    var answerText : [String]
    var corAnswer : String
    
    init(q : String, a : [String], correctAnswer : String ) {
        questionText = q
        answerText = a
        corAnswer = correctAnswer
    }
    
}
