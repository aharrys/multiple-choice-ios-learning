//
//  ViewController.swift
//  Quizzler-iOS13
//
//  Created by Angela Yu on 12/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var seccondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var barProgress: UIProgressView!
    
    var questionDataView = questionData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    @IBAction func senderAnswer(_ sender: UIButton) {
        let senderAnswer = sender.currentTitle!
        let isAnswerTrue = questionDataView.checkAnswer(answer: senderAnswer)
        
        if isAnswerTrue {
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }

        // next Question
        questionDataView.nextQuestion()
        Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    
    }
    
    @objc func updateUI() {
        // setting Button
        let answerChoice = questionDataView.getAnswerText()
        firstButton.setTitle(answerChoice[0], for: .normal)
        seccondButton.setTitle(answerChoice[1], for: .normal)
        thirdButton.setTitle(answerChoice[2], for: .normal)
        
        // Label Score
        scoreLabel.text = "Score : \(questionDataView.getScore())"
        
        questionLabel.text = questionDataView.getQuestionText()
        
        // Progress Bar
        barProgress.progress = questionDataView.getProgressBar()
        
        // Reset Background Color
        firstButton.backgroundColor = UIColor.clear
        seccondButton.backgroundColor = UIColor.clear
        thirdButton.backgroundColor = UIColor.clear
    }
    
}

